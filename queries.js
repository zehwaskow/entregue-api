var promise = require('bluebird');


var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);
var connectionString = 'postgres://ze:8904@localhost:5432/entreguedb';
var db = pgp(connectionString);

// add query functions

module.exports = {
  getAllProducts: getAllProducts,
  getSingleProduct: getSingleProduct,
  createProduct: createProduct,
  updateProduct: updateProduct,
  removeProduct: removeProduct, 
  getAllCustomers: getAllCustomers,
  getSingleCustomer: getSingleCustomer,
  createCustomer: createCustomer,
  updateCustomer: updateCustomer,
  removeCustomer: removeCustomer,
};

//PRODUCTS CRUD
function getAllProducts(req, res, next) {
  db.any('SELECT * FROM product')
    .then(function (data) {
      res.status(200)
        .json({
          data: data,
        });
    })
    .catch(function (err) {
      return next(err);
    })
}


function getSingleProduct(req, res, next) {
  var product_id = parseInt(req.params.product_id);
  db.one('SELECT * FROM product WHERE product_id = $1', product_id)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data   
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function createProduct(req, res, next) {
  req.body.stock = parseInt(req.body.product_stock);
    db.none('INSERT INTO product(product_name, product_brand, product_stock, product_price)'+
    'values(${product_name}, ${product_brand}, ${product_stock}, ${product_price})',
    req.body)
    .then(function () {
      res.status(200)
        .json({
        });
    }) 
    .catch(function (err) {
      return next(err),console.log('TA AQUI ESSA MERDA!!!',req);
    });   
}

function updateProduct(req, res, next) {
  db.none('UPDATE product SET product_name=$1, product_brand=$2, product_stock=$3, product_price=$4,product_image=$5 WHERE product_id=$6',
      [req.body.product_name, req.body.product_brand, parseInt(req.body.product_stock), 
        req.body.product_price,req.body.product_image, parseInt(req.params.product_id)])
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Product Updated Successfully'
        });
    })
    .catch(function (err) {
      return next(err);
    });       
}

function removeProduct(req, res, next) {
  var product_id = parseInt(req.params.product_id);
  db.result('DELETE FROM product WHERE product_id = $1', product_id)
    .then(function (result) {
      res.status(200)
        .json({
          status: 'success',
          message: `Product ${result.rowCount} Removed`
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

//CUSTUMERS CRUD 
function getAllCustomers(req, res, next) {
  db.any('SELECT * FROM customers')
    .then(function (data) {
      res.status(200)
        .json({
          data: data,
        });
    })
    .catch(function (err) {
      return next(err);
    })
}

function getSingleCustomer(req, res, next) {
  var customer_id = parseInt(req.params.customer_id);
  db.one('SELECT * FROM customers WHERE customer_id = $1', customer_id)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data   
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function createCustomer(req, res, next) {
  req.body.customer_number = parseInt(req.body.customer_number);
    db.none('INSERT INTO customers(customer_name, customer_cnpj, customer_telephone,'+ 
    ' customer_cellphone, customer_email, customer_cep, customer_street, customer_number,'+ 
    ' customer_line_two, customer_city, customer_state)'+
    'values(${customer_name}, ${customer_cnpj}, ${customer_telephone}, ${customer_cellphone},'+
    ' ${customer_email}, ${customer_cep}, ${customer_street}, ${customer_number}, ${customer_line_two},'+
    ' ${customer_city}, ${customer_state})',
    req.body)
    .then(function () {
      res.status(200)
        .json({
        });
    }) 
    .catch(function (err) {
      return next(err),console.log('TA AQUI ESSA MERDA!!!',req);
    });   
}

function updateCustomer(req, res, next) {
  db.none('UPDATE product SET customer_name=$1, customer_cnpj=$2,' 
    +'customer_telephone=$3, customer_cellphone=$4, customer_email=$5, customer_cep=$6,'+ 
    ' customer_street=$7, customer_number=$8, customer_line_two=$9, customer_city=$10,'+ 
    ' customer_state=$11 WHERE customer_id = $12',
      [req.body.customer_name, req.body.customer_cnpj, req.body.customer_telephone, 
        req.body.customer_cellphone, req.body.customer_email,req.body.customer_cep, 
        req.body.customer_street,  req.body.customer_number,  req.body.customer_line_two, 
        req.body.customer_city,  req.body.customer_state, parseInt(req.params.customer_id)])
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Product Updated Successfully'
        });
    })
    .catch(function (err) {
      return next(err);
    });       
}

function removeCustomer(req, res, next) {
  var customer_id = parseInt(req.params.customer_id);
  db.result('DELETE FROM customers WHERE customer_id = $1', customer_id)
    .then(function (result) {
      res.status(200)
        .json({
          status: 'success',
          message: `Customer ${result.rowCount} Removed`
        });
    })
    .catch(function (err) {
      return next(err);
    });
}
